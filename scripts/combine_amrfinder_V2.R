require(dplyr)
require(writexl)
require(reshape2)

#loggin
log=file(snakemake@log[[1]],open="wt")
sink(log)
sink(log, type = "message")#logs all output of R into snakemake log

## Read the snakemake amrfiles parameter
tsv_files <- snakemake@input[["amrfiles"]]
# tsv_files <- list.files("results/amrfinderplus",pattern="E.*E.*",full.names=T)

## Read the metadata
metaDF <- read.delim(snakemake@input[["metadata"]],sep="\t", stringsAsFactors = FALSE)
colnames(metaDF)[1] <- "ID"
## Write the distinct gene elements of all strains


## Read all 

# tableList <- lapply(tsv_files,read.delim,as.is=T,header=T,na.strings="-")
tableList <- lapply(tsv_files,read.delim)

## Filter out tables with only header and no rows
idx <- sapply(tableList,nrow) != 0
tableList <- tableList[idx]
tsv_files <- tsv_files[idx]

names(tableList) <- gsub("\\.[a-z]+$","",basename(tsv_files))
tableData <- tableList %>% bind_rows(.id = "ID" ) %>% tibble()
tableData <- tableData[,c(1,3,4,5,6,7,8,10,11,12,13,15,16,17,18,19,20)]
colnames(tableData) <- c("ID","Contig_ID","start","stop","strand","gene.symbol",
                       "gene.name","type","subtype","class","subclass","slength",
                       "qlength","qcov","pid","length","refseqid")

## Remove the contig numbers
tableData$ID <- gsub("(.*)_.+","\\1",tableData$ID)
## Remove features which occur more than once on different contigs
tableData  <- tableData %>% distinct(ID,gene.symbol,.keep_all=TRUE)
write.table(tableData,snakemake@output[["table"]],row.names=F,sep="\t",quote=F)

columnData <- data.frame(tableData[,c("gene.symbol","type","subtype","class","subclass")])
columnData <- data.frame(tableData[,c("gene.symbol","type","class")])
columnData <- columnData %>% arrange(type,class,gene.symbol) %>% distinct(gene.symbol,.keep_all = TRUE)
#sampleIDs <- sort(unique(gsub("(.*)_.+","\\1",tableData$ID)))

pidMatrix <- dcast(tableData,formula=ID ~ gene.symbol,value.var="pid",fill="-")
## Order the pidMatrix according to AMR, Virluence and Alphabetical
cidx <- match(columnData$gene.symbol,colnames(pidMatrix))
pidMatrix <- pidMatrix[,c(1,cidx)]

## Count the number of field unequal empty and minus first column
foundVector <- ncol(pidMatrix) - rowSums(pidMatrix=="-") - 1
foundDF <- data.frame(ID=pidMatrix$ID,NUMB=foundVector)

## 1) Merge meta and found numbers
joinedMetaDF <- merge(metaDF,foundDF,by="ID")

## Add the meta data columns
joinedMetaDF <- merge(joinedMetaDF,pidMatrix,by="ID",all=FALSE)

## Add type and class rows
firstN <- ncol(joinedMetaDF) - nrow(columnData)
firstDF <- cbind(array(NA,c(2,firstN)),t(columnData[,c("type","class")]))
colnames(firstDF) <- colnames(joinedMetaDF)
outputDF <- rbind(firstDF,joinedMetaDF)


## Write to TSV and XLSX
write.table(outputDF,snakemake@output[["alltsv"]],row.names=F,sep="\t",quote=F)
write_xlsx(outputDF,snakemake@output[["allxls"]])
cat("successfully created XLSX \n",file=snakemake@log[[1]],sep="",append=T)


if("multiqc" %in% names(snakemake@output)){
  
  title <- "# title: 'AMRfinder'"
  section <- "# section: ''"
  desc <- "# description: ''"
  format <- "# format: 'tsv'"
  plot_type <- "# plot_type: 'table'"
  configA <- "# pconfig:"
  configB <- "#   format: '{:,.0f}'"
  #config<-"# pconfig:"
  #id<-"#    id: 'Coverage from fastq_info'"
  #ylab<-"#    ylab: 'Coverage'"
  
  
  writeLines(c(title, desc, section, format, plot_type,configA,configB),snakemake@output[["multiqc"]] )
  write.table(outputDF, file=snakemake@output[["multiqc"]], quote=TRUE, sep="\t", row.names=FALSE, col.names=TRUE, append=TRUE)
  cat("successfully created header for MultiQC \n")
  
}
