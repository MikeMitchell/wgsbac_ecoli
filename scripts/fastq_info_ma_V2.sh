#!/bin/bash
#dependencies
#mash
#seqkit
fastq1=$1  #fastq input 1
fastq2=$2  #fastq input 2
fasta=$3 #fasta input

readcount1=$(scripts/zbzcat.sh $fastq1| echo $((`wc -l`/4))) #read count in file 1
readcount2=$(scripts/zbzcat.sh $fastq2| echo $((`wc -l`/4))) #read count in file 2
total_seq=$(echo "$readcount1+$readcount2"|bc) #calculate total amount of  reads

readlength1=$( scripts/zbzcat.sh $fastq1 | awk 'NR % 4 == 2 { s += length($1); t++} END {print s/t}') #calculate read length for fastq file 1
readlength2=$( scripts/zbzcat.sh | awk 'NR % 4 == 2 { s += length($1); t++} END {print s/t}' $fastq2) #calculate read length for fastq file 2
totalreadlength=$(echo "$readlength1+$readlength2"|bc) #calculate total read length
averagereadlength=$(echo "$totalreadlength/2"|bc) #calculate average read length by parsing into bc command - which truncates the decimal numbers by default in multiplication/division
#To calculate genome size
genomesize=$(grep -v '^>' $fasta |wc -c)

coverage=$(echo "$total_seq*$averagereadlength/$genomesize"|bc)

filenameshort=$(basename -- "$fastq1")
filenameshort2=$(basename -- "$fastq2")
filenameshort_ref=$(basename -- "$fasta")



#To print the tab-delimited table on standard output
echo -n -e "File R1\t"
echo -n -e "File R2\t"
echo -n -e "Reference Genome\t"
echo -n -e "Reference genome size\t"
echo -n -e "Average read length\t"
echo -n -e "Total reads\t"
echo -e "Theoretical coverage"
echo -n -e "$filenameshort\t"
echo -n -e "$filenameshort2\t"
echo -n -e "$filenameshort_ref\t"
echo -n -e "$genomesize\t"
echo -n -e "$averagereadlength\t"
echo -n -e "$total_seq\t"
echo -e "$coverage"


exit 0;
