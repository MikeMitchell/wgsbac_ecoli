

for row in samples_fasta.itertuples():
  os.makedirs("results/finalAssembly", exist_ok=True)
  src = row.File_assembly
  dst = "results/finalAssembly/" + row.ID + ".fasta"
  if not os.path.exists(dst):
    os.symlink(src, dst)
