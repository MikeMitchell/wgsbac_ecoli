#!/usr/bin/env Rscript

#logs all output of R into snakemake log
log=file(snakemake@log[[1]],open="wt")
sink(log)
sink(log, type = "message")

require(dplyr)

## serofinder Data Text output file
itolFile <- snakemake@output[["itol"]]

## Add ITOL File Text
seroDF <- read.delim(snakemake@input[["sero"]],sep="\t", stringsAsFactors = FALSE)
colnames(seroDF)[1] <- "ID"

seroDF$pos <- "-1"
seroDF$color <- "#000000"
seroDF$style <- "normal"
seroDF$sizeFactor <- "1"
seroDF$rotation <- "0"
#9606,Homo sapiens,-1,#ff0000,bold,2,0

cat("DATASET_TEXT","\n",file=itolFile)
cat("SEPARATOR TAB","\n",file=itolFile,append=T)
cat("COLOR","#ff0000","\n",sep="\t",file=itolFile,append=T)
cat("DATASET_LABEL","Serotype","\n",sep="\t",file=itolFile,append=T)
cat("MARGIN","1","\n",sep="\t",file=itolFile,append=T)
cat("DATA","\n",file=itolFile,append=T)
write.table(seroDF,itolFile,row.names=F,quote=F,sep="\t",append=T)
