import pandas as pd

## Reference:
## https://git-r3lab.uni.lu/aurelien.ginolhac/snakemake-workflows/-/tree/master/workflow

def get_fastq(wildcards):
    return samples_fastq.loc[(wildcards.fastq), ["File_left_reads","File_right_reads"]].dropna()
    
def get_fasta(wildcards):
    return samples_fasta.loc[(wildcards.sample), ["File_assembly"]].dropna()


rule link_samples_fastq:
  input:
    get_fastq
  output:
    fw="input/{fastq}_R1.fastq.gz",
    rv="input/{fastq}_R2.fastq.gz"
  shell:
    """
    ln -s {input[0]} {output.fw} 
    ln -s {input[1]} {output.rv}
    """


# Input: list of already present assembled fastas

#rule link_samples_fasta:
#  input:
#    get_fasta
#  output:
#    "results/finalAssembly/{sample}.fasta",
#  shell:
#    """
#    ln -s {input[0]} {output} 
#    """    
    