

"""
Clermont phylogrup identification with EZClermont

"""

#input is final assemblies after spades and filtering with coverage and length
#directory holding all mlst results for each sample

rule clermont:
    input:
        "results/finalAssembly/{sample}.fasta"
    output:
        "results/clermont/{sample}.clermont.txt",
    log:
        "log/clermont/{sample}.log"
    benchmark:
        "benchmark/clermont/{sample}.txt"
    conda:
        "../envs/clermont.yaml"
    threads: 1
    shell:
        """
        set +e
        ezclermont {input} 1>{output} 2>{log} || true
        """


rule clermont_summary:
	input:
		expand("results/clermont/{sample}.clermont.txt", sample=samples.ID)
	output:
		sum="results/clermont/ezclermont_summary.tsv",
		mqc="results/clermont/ezclermont_mqc.txt"
	log:
		"log/clermont/clermont_summary.log"
	shell:
		"""
		cat {input} 1> {output.sum} 2>{log}
		Rscript --verbose scripts/combine_ezclermont.R {output.sum} {output.mqc} > {log}
		"""