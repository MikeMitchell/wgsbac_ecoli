
"""
Download Serotypefinder DB

"""

rule download_serotypefinder_db:
  output:
    "results/abricatedb/serotypefinder/O_type.fsa",
    "results/abricatedb/serotypefinder/H_type.fsa",
    "results/abricatedb/serotypefinder/sequences"
  log:
    "log/serotypefinder/serotypefinder_download.log"
  benchmark:
    "benchmark/serotype/download_serotype.txt"
  conda:
    "../envs/abricate.yaml"
  shell:
        """
        sh scripts/download_serotypefinder_db.sh
        """
        
rule abricate_serotypefinder:
    input:
        fasta="results/finalAssembly/{sample}.fasta",
        db="results/abricatedb/serotypefinder/sequences"
    output:
        serofinder="results/abricate/serotypefinder/{sample}_serotypefinder.tsv",
    log:
        "log/abricate/{sample}_serotypefinder.log"
    threads: 4
    benchmark:
        "log/benchmark/abricate/{sample}_serotypefinder.txt"
    conda:
      "../envs/abricate.yaml"
    params:
      dbdir="results/abricatedb"
    shell:
      "abricate --datadir {params.dbdir} --threads {threads} {input.fasta}  --db serotypefinder --minid 90  > {output.serofinder} 2>>{log}"
      

"""
Combines/summarizes Abricate results in one single tsv
"""
rule summary_abricate_serotypefinder:
    input:
        expand("results/abricate/serotypefinder/{sample}_serotypefinder.tsv",sample=samples.ID)
    output:
        temp("tmp/abricate/serotypefinder.tsv")
    log:
        "log/abricate/combine_abricate_serotypefinder.log"
    benchmark:
        "log/benchmark/abricate/combine_abricate_serotypefinder.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate  --summary  {input}   >{output} 2>>{log}"
        
        
rule collect_abbricate_serotypefinder:
  input:
      abricateres="tmp/abricate/serotypefinder.tsv",
      metadata=config['metadata']
  output:
      abricate_tsv="results/abricate/all_serofinder.tsv",
      abricate_xlsx="results/abricate/all_serofinder.xlsx",
      abricate_mqc="results/abricate/Abricate_serofinder_mqc.txt"
  params:
      db="serotypefinder"
  log:
      "log/abricate/collect_abbricate_serotypefinder.log"
  benchmark:
      "log/benchmark/abricate/collect_abbricate_serotypefinder.txt"
  conda:
      "../envs/rxls.yaml"
  script:
      "../scripts/combine_abricate_serofinder.R"
     
      
