


"""
Calculate coverage for each sample
"""

#adapted from https://github.com/raymondkiu/fastq-info/blob/master/fastq_info_3.sh
#input are zipped fastq-files AND reference genome assembly

rule coverage:
    input:
      fw="input/{fastq}_R1.fastq.gz",
      rv="input/{fastq}_R2.fastq.gz",
      ref=config["refstrain"]
    output:
      out="results/coverage/{fastq}.csv"
    log:
       "log/coverage/{fastq}.log"
    shell:
       "sh scripts/fastq_info_ma_V2.sh  {input.fw} {input.rv} {input.ref} > {output.out} 2>{log}"

"""
Collects coverage result from ech sample and combines in one file
"""
rule collect_coverage:
    input:
        localres=expand("results/coverage/{fastq}.csv", fastq=samples_fastq.ID),
        metadata=config['metadata']
    output:
        allcsv="results/coverage/allcoverage.csv",
        allxls="results/coverage/allcoverage.xlsx",
        multiqc="results/coverage/Coverage_fastq_info_mqc.txt"
    log:
       "log/coverage/allcoverage.log"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/combineCoverage_V2.R"
