

"""
MLST alleles detection from WGS data using mlst tool (classical, 6-10 alleles)

"""
#input is final assemblies after spades and filtering with coverage and length
#output directory holding all mlst results for each sample

rule mlst:
    input:
        "results/finalAssembly/{sample}.fasta"
    output:
        "results/mlst/{sample}.mlst.txt",
    log:
        "log/mlst/{sample}.log"
    benchmark:
        "log/benchmark/mlst/{sample}.txt"
    conda:
        "../envs/mlst.yaml"
    shell:
        "mlst {input} --nopath  1> {output} 2>{log}"


"""
Combines single MLST results and converst into XLS and MQC report
"""

#outpus as csv and xls
rule collect_mlst:
    input:
        localres= expand("results/mlst/{sample}.mlst.txt", sample=samples.ID),
        metadata=config['metadata'],
    output:
        allcsv="results/mlst/allMLST.csv",
        allxls="results/mlst/allMLST.xlsx",
        multiqc="results/mlst/Classical_MLST_mqc.txt",
    log:
        "log/mlst/collect_mlst.log"
    benchmark:
        "log/benchmark/mlst/collect_mlst.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/combineMLST.R"
