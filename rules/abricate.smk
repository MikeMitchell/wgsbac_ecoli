
"""
Runs Abricate using assembly as input against following databases:
-resfinder
"""
rule abricate_resfinder:
    input:
        "results/finalAssembly/{sample}.fasta"
    output:
        "results/abricate/{sample}_resfinder.tsv",
    log:
        "log/abricate/{sample}_resfinder.log"
    threads: 4
    benchmark:
        "log/benchmark/abricate/{sample}_resfinder.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate --threads {threads} {input}  --db resfinder  >{output} 2>>{log}"



"""
Runs Abricate using assembly as input against following databases:
-card
"""
rule abricate_card:
    input:
        "results/finalAssembly/{sample}.fasta",
    output:
        "results/abricate/{sample}_card.tsv",
    log:
        "log/abricate/{sample}_card.log"
    threads: 4
    benchmark:
        "log/benchmark/abricate/{sample}_card.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate --threads {threads} {input}  --db card  >{output} 2>>{log}"

"""
Runs Abricate using assembly as input against following databases:
-ncbi
"""
rule abricate_ncbi:
    input:
        "results/finalAssembly/{sample}.fasta",
    output:
        "results/abricate/{sample}_ncbi.tsv",
    log:
        "log/abricate/{sample}_ncbi.log"
    threads: 4
    benchmark:
        "log/benchmark/abricate/{sample}_ncbi.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate --threads {threads} {input}  --db card  >{output} 2>>{log}"



"""
Runs Abricate using assembly as input against following databases:
--vfdb
"""
rule abricate_vfdb:
    input:
        "results/finalAssembly/{sample}.fasta",
    output:
        "results/abricate/{sample}_vfdb.tsv",
    log:
        "log/abricate/{sample}_vfdb.log"
    threads: 4
    benchmark:
        "log/benchmark/abricate/{sample}_vfdb.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
       "abricate --threads {threads} {input}  --db vfdb  >{output} 2>>{log}"

"""
Runs Abricate using assembly as input against following databases:
--vfdb
"""
rule abricate_plasmidfinder:
    input:
        "results/finalAssembly/{sample}.fasta"
    output:
        "results/abricate/{sample}_plasmidfinder.tsv"
    log:
        "log/abricate/{sample}_plasmidfinder.log"
    threads: 4
    benchmark:
        "log/benchmark/abricate/{sample}_plasmidfinder.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
       "abricate --threads {threads} {input}  --db plasmidfinder  >{output} 2>>{log}"





"""
Combines/summarizes Abricate results in one single tsv
"""
rule summary_abricate_plasmidfinder:
    input:
        expand("results/abricate/{sample}_plasmidfinder.tsv", sample=samples.ID),
    output:
        temp("tmp/abricate/plasmidfinder.tsv"),
    log:
        "log/abricate/combine_abricate_plasmidfinder.log"
    benchmark:
        "log/benchmark/abricate/combine_abricate_plasmidfinder.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate  --summary  {input}   >{output} 2>>{log}"



"""
Combines/summarizes Abricate results in one single tsv
"""
rule summary_abricate_resfinder:
    input:
        expand("results/abricate/{sample}_resfinder.tsv", sample=samples.ID),
    output:
        temp("tmp/abricate/resfinder.tsv"),
    log:
        "log/abricate/combine_abricate_vfdb.log"
    benchmark:
        "log/benchmark/abricate/combine_abricate_vfdbtxt"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate  --summary  {input}   >{output} 2>>{log}"


"""
Combines/summarizes Abricate results in one single tsv
"""
rule summary_abricate_vfdb:
    input:
        expand("results/abricate/{sample}_vfdb.tsv", sample=samples.ID),
    output:
        temp("tmp/abricate/vfdb.tsv"),
    log:
        "log/abricate/combine_abricate_vfdb.log"
    benchmark:
        "log/benchmark/abricate/combine_abricate_vfdb.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate  --summary  {input}   >{output} 2>>{log}"

"""
Combines/summarizes Abricate results in one single tsv
"""

rule summary_abricate_card:
    input:
        expand("results/abricate/{sample}_card.tsv", sample=samples.ID),
    output:
        temp("tmp/abricate/card.tsv"),
    log:
        "log/abricate/combine_abricate_card.log"
    benchmark:
        "log/benchmark/abricate/combine_abricate_card.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate  --summary  {input}   >{output} 2>>{log}"
"""
Combines/summarizes Abricate results in one single tsv
"""
rule summary_abricate_ncbi:
    input:
        expand("results/abricate/{sample}_ncbi.tsv", sample=samples.ID),
    output:
        temp("tmp/abricate/ncbi.tsv"),
    log:
        "log/abricate/combine_abricate_ncbi.log"
    benchmark:
        "log/benchmark/abricate/combine_abricate_ncbi.txt"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate  --summary  {input}   >{output} 2>>{log}"




"""
Converts combined abbricate plasmidfinder results in XLS and MQC report
"""

rule collect_abricate_ncbi:
    input:
        abricateres="tmp/abricate/ncbi.tsv",
        metadata=config['metadata']
    output:
        abricate_tsv="results/abricate/all_ncbi.tsv",
        abricate_xls="results/abricate/all_ncbi.xls",
        abricate_mqc="results/abricate/Abricate_ncbi_mqc.txt"
    params:
        db="ncbi"
    log:
        "log/abricate/collect_abbricate_ncbi.log"
    benchmark:
        "log/benchmark/abricate/collect_abbricate_ncbi.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/collect_abricate.R"

"""
Converts combined abbricate plasmidfinder results in XLS and MQC report
input: temp file after abricate summary
output: tsv xls mqc format
"""

rule collect_abbricate_card:
    input:
        abricateres="tmp/abricate/card.tsv",
        metadata=config['metadata']
    output:
        abricate_tsv="results/abricate/all_card.tsv",
        abricate_xls="results/abricate/all_card.xls",
        abricate_mqc="results/abricate/abricate_card_mqc.txt"
    params:
        db="card"
    log:
        "log/abricate/collect_abbricate_card.log"
    benchmark:
        "log/benchmark/abricate/collect_abbricate_card.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls





"""
Converts combined abbricate plasmidfinder results in XLS and MQC report
input: temp file after abricate summary
output: tsv xls mqc format
"""


rule collect_abbricate_vfdb:
    input:
        abricateres="tmp/abricate/vfdb.tsv",
        metadata=config['metadata']
    output:#outpus as tsv and xls
        abricate_tsv="results/abricate/all_vfdb.tsv",
        abricate_xls="results/abricate/all_vfdb.xls",
        abricate_mqc="results/abricate/Abricate_vfdb_mqc.txt"
    params:
        db="vfdb"
    log:
        "log/abricate/collect_abbricate_vfdb.log"
    benchmark:
        "log/benchmark/abricate/collect_abbricate_vfdb.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/collect_abricate.R"#converts to xls


"""
Converts combined abbricate plasmidfinder results in XLS and MQC report
input: temp file after abricate summary
output: tsv xls mqc format
"""

rule collect_abbricate_plasmidfinder:
    input:#input is
        abricateres="tmp/abricate/plasmidfinder.tsv",
        metadata=config['metadata']
    output:
        abricate_tsv="results/abricate/all_plasmidfinder.tsv",
        abricate_xls="results/abricate/all_plasmidfinder.xls",
        abricate_mqc="results/abricate/Abricate_Plasmidfinder_mqc.txt"
    params:
        db="plasmidfinder"
    log:
        "log/abricate/collect_abbricate.log"
    benchmark:
        "log/benchmark/abricate/collect_abbricate.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/collect_abricate.R"


"""
Converts combined abbricate resfinder results in XLS and MQC report
"""

#input: temp file after abricate summary
#output: tsv xls mqc format

rule collect_abbricate_resfinder:
    input:
        abricateres="tmp/abricate/resfinder.tsv",
        metadata=config['metadata'],
    output:#outpus as tsv and xls
        abricate_tsv="results/abricate/all_resfinder.tsv",
        abricate_xls="results/abricate/all_resfinder.xls",
        abricate_mqc="results/abricate/Abricate_Resfinder_mqc.txt"
    params:
        db="resfinder"
    log:
        "log/abricate/collect_abbricate.log"
    benchmark:
        "log/benchmark/abricate/collect_abbricate.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/collect_abricate.R"
        
        

