
"""
Runs Abricate using assembly as input against following databases:
-resfinder
"""
rule abricate_plasmid_resfinder:
    input:
        "results/spades_plasmid/{sample}"
    output:
        "results/abricate_plasmid/resfinder/{sample}_resfinder.tsv"
    log:
        "log/abricate_plasmid/{sample}_resfinder.log"
    threads: 4
    conda:
      "../envs/abricate.yaml"
    shell:
      """
        if [[ -f {input}/contigs.fasta ]]
        then 
            abricate --threads {threads} {input}/contigs.fasta --db resfinder  >{output} 2>>{log}
        else 
            touch {output}
        fi
      """

"""
Runs Abricate using assembly as input against following databases:
-resfinder
"""
rule abricate_plasmid_vfdb:
    input:
        "results/spades_plasmid/{sample}"
    output:
        "results/abricate_plasmid/vfdb/{sample}_vfdb.tsv"
    log:
        "log/abricate_plasmid/{sample}_vfdb.log"
    threads: 4
    conda:
      "../envs/abricate.yaml"
    shell:
      """
        if [[ -f {input}/contigs.fasta ]]
        then 
            abricate --threads {threads} {input}/contigs.fasta --db vfdb  >{output} 2>>{log}
        else 
            touch {output}
        fi
      """

"""
Runs Abricate using assembly as input against following databases:
-resfinder
"""
rule abricate_plasmid_plasmidfinder:
    input:
        "results/spades_plasmid/{sample}"
    output:
        "results/abricate_plasmid/plasmidfinder/{sample}_plasmidfinder.tsv"
    log:
        "log/abricate_plasmid/{sample}_plasmidfinder.log"
    threads: 4
    conda:
      "../envs/abricate.yaml"
    shell:
      """
        if [[ -f {input}/contigs.fasta ]]
        then 
            abricate --threads {threads} {input}/contigs.fasta --db plasmidfinder  >{output} 2>>{log}
        else 
            touch {output}
        fi
      """



"""
Combines/summarizes Abricate results in one single tsv
"""
rule summary_abricate_plasmid_resfinder:
    input:
        expand("results/abricate_plasmid/resfinder/{sample}_resfinder.tsv", sample=config["fastq"]),
    output:
        temp("results/abricate_plasmid/resfinder_summary.tsv")
    log:
        "log/abricate_plasmid/combine_abricate_vfdb.log"
    benchmark:
        "log/benchmark/abricate_plasmid/combine_abricate_plasmid"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate  --summary  {input} > {output} 2>>{log}"
      

rule summary_abricate_plasmid_vfdb:
    input:
        expand("results/abricate_plasmid/vfdb/{sample}_vfdb.tsv", sample=samples.ID),
    output:
        temp("results/abricate_plasmid/vfdb_summary.tsv")
    log:
        "log/abricate_plasmid/combine_abricate_vfdb.log"
    benchmark:
        "log/benchmark/abricate_plasmid/combine_abricate_plasmid"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate  --summary  {input} > {output} 2>>{log}"


rule summary_abricate_plasmid_plasmidfinder:
    input:
        expand("results/abricate_plasmid/plasmidfinder/{sample}_plasmidfinder.tsv", sample=samples.ID),
    output:
        temp("results/abricate_plasmid/plasmidfinder_summary.tsv")
    log:
        "log/abricate_plasmid/combine_abricate_vfdb.log"
    benchmark:
        "log/benchmark/abricate_plasmid/combine_abricate_plasmid"
    conda:
      "../envs/abricate.yaml"
    shell:
      "abricate  --summary  {input} > {output} 2>>{log}"


"""
Converts combined abbricate resfinder results in XLS and MQC report
"""
rule collect_abricate_plasmid_resfinder:
    input:
        resfinder=expand("results/abricate_plasmid/resfinder/{sample}_resfinder.tsv", sample=samples.ID),
        abricateres="results/abricate_plasmid/resfinder_summary.tsv",
        metadata=config['metadata']
    output:
        abricate_tsv="results/abricate_plasmid/all_resfinder.tsv",
        abricate_xls="results/abricate_plasmid/all_resfinder.xls"
    params:
        db="resfinder"
    log:
        "log/abricate_plasmid/collect_abbricate.log"
    benchmark:
        "log/benchmark/abricate_plasmid/collect_abbricate.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/collect_abricate.R"

"""
Converts combined abbricate plasmidfinder results in XLS
"""
rule collect_abricate_plasmid_vfdb:
    input:
        abricateres="results/abricate_plasmid/vfdb_summary.tsv",
        metadata=config['metadata']
    output:
        abricate_tsv="results/abricate_plasmid/all_vfdb.tsv",
        abricate_xls="results/abricate_plasmid/all_vfdb.xls"
    params:
        db="vfdb"
    log:
        "log/abricate_plasmid/collect_abbricate_vfdb.log"
    benchmark:
        "log/benchmark/abricate_plasmid/collect_abbricate_vfdb.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/collect_abricate.R"
        
        
"""
Converts combined abbricate plasmidfinder results in XLS
"""
rule collect_abricate_plasmid_plasmidfinder:
    input:
        abricateres="results/abricate_plasmid/plasmidfinder_summary.tsv",
        metadata=config['metadata']
    output:
        abricate_tsv="results/abricate_plasmid/all_plasmidfinder.tsv",
        abricate_xls="results/abricate_plasmid/all_plasmidfinder.xls"
    params:
        db="plasmidfinder"
    log:
        "log/abricate_plasmid/collect_abbricate_plasmidfinder.log"
    benchmark:
        "log/benchmark/abricate_plasmid/collect_abbricate_plasmidfinder.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/collect_abricate.R"        

