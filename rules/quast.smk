
"""
Quast for statistics about all final assemblies
"""
# input is assembly files after filtering with coverage and length AND Reference genoem file
# collect_quast creates directory for each sample inlcuding tsv html ,...

rule quast:
    input:
        fasta="results/finalAssembly/{sample}.fasta",
        ref=config["refstrain"]
    output:
        directory("results/quast/{sample}")
    benchmark:
        "log/benchmark/quast/{sample}.txt"
    log:
        "log/quast/{sample}.txt"
    conda:
      "../envs/quast.yaml"
    shell:
       "quast.py -o {output} -R {input.ref} {input.fasta}  &>  {log} || true"


"""
Quast for statistics about all final assemblies
"""

#input is assembly files after filtering with coverage and length AND Reference genoem file
#collect_quast creates directory for each sample inlcuding tsv html ,...

rule quast_single:
    input:
        fasta=expand("results/finalAssembly/{sample}.fasta", sample=samples.ID),
        ref=config["refstrain"]
    output:
        outdir1=directory("results/quast/quast"),
        outdir2=directory("results/quast/quast_norf")
    benchmark:
        "log/benchmark/quast/quast_single.txt"
    log:
        "log/quast/quast_log.txt"
    conda:
      "../envs/quast.yaml"
    shell:
    	"""
	      quast.py -o {output.outdir1} -R {input.ref} {input.fasta} &> {log}
	      quast.py -o {output.outdir2} {input.fasta} &> {log}
    	"""



"""
Collects Quality of Assemblies
"""
# input is quast results for each sample
# output is csv and xls
rule collect_quast:
    input:
        localres=expand("results/quast/{sample}", sample=samples.ID),
    output:
        allcsv="results/quast/allquast.csv",
        allxls="results/quast/allquast.xlsx",
    benchmark:
        "log/benchmark/quast/allquast.txt",
    log:
        "log/quast/allquast.txt",
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/combinequast.R"


