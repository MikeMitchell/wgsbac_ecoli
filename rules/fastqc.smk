"""
Quality controll FASTQC
"""

#receive all forward and reverse fastq files
#create output directories before running fastqc for each forward and reverse reads seperately

rule fastqc:
    input:
        fw="input/{fastq}_R1.fastq.gz",
        rv="input/{fastq}_R2.fastq.gz"
    output:
        directory("results/fastqc/{fastq}")
    threads: 16
    benchmark:
        "log/benchmark/fastqc/{fastq}.txt"
    log:
        "log/fastqc/{fastq}.log"
    conda:
      "../envs/fastqc.yaml"
    shell:
        "mkdir -p {output} && fastqc -t {threads} -o {output} {input.fw} {input.rv}  &> {log}"
